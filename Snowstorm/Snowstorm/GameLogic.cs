﻿// <copyright file="GameLogic.cs" company="Obuda"> 
//      Obuda copyright tag.
// </copyright>
// <summary>
//  <author>Meruyert Bekzhigit</author>
// </summary>
namespace Snowstorm
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Shapes;
    using System.Windows.Threading;

    /// <summary>
    /// Some additional functions for figure creation.
    /// </summary>
    public class GameLogic
    {
        /// <summary>
        /// View Model.
        /// </summary>
        private ViewModel viewModel;

        /// <summary>
        /// Game Model.
        /// </summary>
        private GameModel gameModel;

        /// <summary>
        /// Dispatcher Timer to create smooth effect.
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Counter for every tick.
        /// </summary>
        private int cnt;

        /// <summary>
        /// Distance Player snowball should fly.
        /// </summary>
        private double distanceX;

        /// <summary>
        /// Distance AI snowball should fly.
        /// </summary>
        private double aidistanceX;

        /// <summary>
        /// AI's target.
        /// </summary>
        private Player target;

        /// <summary>
        /// Initializes a new instance of the GameLogic class.
        /// </summary>
        /// <param name="viewModel">View Model for Game Logic.</param>
        /// <param name="gameModel">Game Model.</param>
        public GameLogic(ViewModel viewModel, GameModel gameModel)
        {
            this.viewModel = viewModel;
            this.gameModel = gameModel;

            this.InitiateAI();

            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromMilliseconds(50);
            this.timer.Tick += this.Timer_Tick;
            this.timer.Start();
        }

        /// <summary>
        /// Selects Player from First Team.
        /// </summary>
        /// <param name="mousePoint">Mouse position.</param>
        /// <returns>Whether Player is selected or not.</returns>
        public bool SelectPlayer(Point mousePoint)
        {
            foreach (Player player in this.viewModel.FirstTeam)
            {
                if (player.Area.Contains(mousePoint))
                {
                    player.State = States.Moving;
                    player.CreateBall();
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Unselects Player from First Team.
        /// </summary>
        /// <returns>Whether Player is unselected or not.</returns>
        public bool UnselectPlayerAndShoot()
        {
            foreach (Player player in this.viewModel.FirstTeam)
            {
                if (player.State == States.Moving)
                {
                    player.State = States.Shoot;
                    this.distanceX = 0;
                    switch (player.SnowBall.Power)
                    {
                        case 1:
                            this.distanceX = player.SnowBall.Area.X - 40;
                            break;
                        case 2:
                            this.distanceX = player.SnowBall.Area.X - 120;
                            break;
                        case 3:
                            this.distanceX = player.SnowBall.Area.X - 200;
                            break;
                        case 4:
                            this.distanceX = player.SnowBall.Area.X - 300;
                            break;
                    }

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Moves Player from First Team.
        /// </summary>
        /// <param name="mousePoint">Mouse position.</param>
        public void MovePlayer(Point mousePoint)
        {
            foreach (Player player in this.viewModel.FirstTeam)
            {
                if (player.State == States.Moving)
                {
                    if (mousePoint.X > (this.gameModel.Width / 2) + 15)
                    {
                        player.Area.Y = mousePoint.Y - 10;
                        player.Area.X = mousePoint.X - 10;
                        player.SnowBall.Area.Y = player.Area.Y * 0.91;
                        player.SnowBall.Area.X = player.Area.X + player.Area.Width;
                    }
                }
            }
        }

        /// <summary>
        /// Method which starts AI.
        /// </summary>
        public void InitiateAI()
        {
            foreach (Player ai in this.viewModel.SecondTeam)
            {
                if (ai.State == States.Rest)
                {
                    ai.State = States.Moving;
                    ai.CreateBall();
                }
            }
        }

        /// <summary>
        /// Moves Player from Second Team.
        /// </summary>
        /// <param name="player">Target Player.</param>
        public void MoveAI(Player player)
        {
            if (player == null)
            {
                return;
            }

            foreach (Player ai in this.viewModel.SecondTeam)
            {
                if (ai.State == States.Moving)
                {
                    if ((int)ai.Area.Y > (int)player.Area.Y + 10)
                    {
                        ai.Area.Y -= 1;
                        ai.SnowBall.Area.Y = ai.Area.Y - ai.SnowBall.Area.Height;
                        ai.SnowBall.Area.X = ai.Area.X - ai.SnowBall.Area.Width;
                    }
                    else if ((int)ai.Area.Y < (int)player.Area.Y + 10)
                    {
                        ai.Area.Y += 1;
                        ai.SnowBall.Area.Y = ai.Area.Y - ai.SnowBall.Area.Height;
                        ai.SnowBall.Area.X = ai.Area.X - ai.SnowBall.Area.Width;
                    }
                    else
                    {
                        if (ai.SnowBall.Power == 4)
                        {
                            ai.State = States.Shoot;

                            this.aidistanceX = 0;
                            switch (ai.SnowBall.Power)
                            {
                                case 1:
                                    this.aidistanceX = ai.SnowBall.Area.X + 40;
                                    break;
                                case 2:
                                    this.aidistanceX = ai.SnowBall.Area.X + 120;
                                    break;
                                case 3:
                                    this.aidistanceX = ai.SnowBall.Area.X + 200;
                                    break;
                                case 4:
                                    this.aidistanceX = ai.SnowBall.Area.X + 300;
                                    break;
                            }
                        }
                    }

                    this.viewModel.Refresh();
                }
            }
        }

        /// <summary>
        /// Shoots Snowball of SelectedPlayer from First Team.
        /// </summary>
        public void SnowBallShoot()
        {
            //// Player Snowball Shoot.
            foreach (Player player in this.viewModel.FirstTeam)
            {
                if (player.State == States.Shoot && player.SnowBall != null)
                {
                    if (player.SnowBall.Area.X > this.distanceX)
                    {
                        player.SnowBall.Area.X -= SnowBall.SpeedX;
                        this.viewModel.Refresh();
                    }
                    else
                    {
                        player.State = States.Rest;
                        player.SnowBall = null;
                        this.viewModel.Refresh();
                    }
                }
            }

            //// AI Snowball Shoot.
            foreach (Player ai in this.viewModel.SecondTeam)
            {
                if (ai.State == States.Shoot && ai.SnowBall != null)
                {
                    if (ai.SnowBall.Area.X < this.aidistanceX)
                    {
                        ai.SnowBall.Area.X += SnowBall.SpeedX;
                        this.viewModel.Refresh();
                    }
                    else
                    {
                        ai.State = States.Rest;
                        ai.SnowBall = null;
                        this.viewModel.Refresh();
                    }
                }
            }
        }

        /// <summary>
        /// Function which checks whether Player was shot by ai.
        /// </summary>
        public void CheckPlayerOnTouch()
        {
            foreach (Player player in this.viewModel.FirstTeam)
            {
                if (!player.IsAlive)
                {
                    this.viewModel.FirstTeam.Remove(player);
                    this.viewModel.Refresh();
                    break;
                }

                foreach (Player ai in this.viewModel.SecondTeam)
                {
                    if (ai.SnowBall != null)
                    {
                        if (player.Area.IntersectsWith(ai.SnowBall.Area))
                        {
                            player.Armor -= 1;
                            ai.State = States.Rest;
                            ai.SnowBall = null;

                            this.target = null;
                            this.viewModel.Refresh();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Function which checks whether AI was shot by Player.
        /// </summary>
        public void CheckAIOnTouch()
        {
            foreach (Player ai in this.viewModel.SecondTeam)
            {
                if (!ai.IsAlive)
                {
                    this.viewModel.SecondTeam.Remove(ai);
                    this.viewModel.Refresh();
                    break;
                }

                foreach (Player player in this.viewModel.FirstTeam)
                {
                    if (player.SnowBall != null)
                    {
                        if (ai.Area.IntersectsWith(player.SnowBall.Area))
                        {
                            ai.Armor -= 1;
                            player.SnowBall = null;
                            player.State = States.Rest;
                            this.viewModel.Refresh();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Method to check who won the game.
        /// </summary>
        public void CheckTheWinner()
        {
            if (this.viewModel.FirstTeam.Count == 0 && this.viewModel.SecondTeam.Count > 0)
            {
                MessageBox.Show("Sorry but you LOST! Try again.");
                this.timer.Stop();
                return;
            }

            if (this.viewModel.SecondTeam.Count == 0 && this.viewModel.FirstTeam.Count > 0)
            {
                MessageBox.Show("You win! Try again");
                this.timer.Stop();
                return;
            }
        }

        /// <summary>
        /// Tick method of timer.
        /// </summary>
        /// <param name="sender">Object which fires event.</param>
        /// <param name="e">Arguments of Objects.</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            this.cnt++;
            if (this.cnt >= 20)
            {
                this.InitiateAI();

                //// Increasing Power of Player
                foreach (Player player in this.viewModel.FirstTeam)
                {
                    if (player.State == States.Moving && player.SnowBall.Power < 4)
                    {
                        player.SnowBall.Power++;
                        this.viewModel.Refresh();
                    }
                }

                //// Increasing Power of AI
                foreach (Player ai in this.viewModel.SecondTeam)
                {
                    if (ai.State == States.Moving && ai.SnowBall.Power < 4)
                    {
                        ai.SnowBall.Power++;
                        this.viewModel.Refresh();
                    }
                }

                this.cnt = 0;
            }

            this.SnowBallShoot();
            this.CheckAIOnTouch();
            this.CheckPlayerOnTouch();

            foreach (Player player in this.viewModel.FirstTeam)
            {
                if (this.target == null && player.IsAlive)
                {
                    this.target = player;
                }
            }

            this.MoveAI(this.target);
            this.CheckTheWinner();
        }
    }
}
