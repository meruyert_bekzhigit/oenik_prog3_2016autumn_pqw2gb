﻿// <copyright file="GameModel.cs" company="Obuda"> 
//      Obuda copyright tag.
// </copyright>
// <summary>
//  <author>Meruyert Bekzhigit</author>
// </summary>
namespace Snowstorm
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// GameModel class with properties which describes game.
    /// </summary>
    public class GameModel
    {
        /// <summary>
        /// Declaring integer with number of First Team Players.
        /// </summary>
        private int firstTeamPlayers;

        /// <summary>
        /// Declaring integer with number of Second Team Players.
        /// </summary>
        private int secondTeamPlayers;

        /// <summary>
        /// Declaring double with width of Window.
        /// </summary>
        private double width;
                
        /// <summary>
        /// Declaring double with height of Window.
        /// </summary>
        private double height;

        /// <summary>
        /// Initializes a new instance of the GameModel class.
        /// </summary>
        /// <param name="width">Width of Window.</param>
        /// <param name="height">Height of Window.</param>
        public GameModel(double width, double height)
        {
            this.FirstTeamPlayers = 3;
            this.SecondTeamPlayers = 3;

            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// Gets or sets number of firstTeamPlayers of GameModel.
        /// </summary>
        /// <value>The number of first team players.</value>
        public int FirstTeamPlayers
        {
            get { return this.firstTeamPlayers; }
            set { this.firstTeamPlayers = value; }
        }

        /// <summary>
        /// Gets or sets number of secondTeamPlayers of GameModel.
        /// </summary>
        /// <value>The number of second team players.</value>
        public int SecondTeamPlayers
        {
            get { return this.secondTeamPlayers; }
            set { this.secondTeamPlayers = value; }
        }

        /// <summary>
        /// Gets or sets width of GameModel.
        /// </summary>
        /// <value>The Width of Window.</value>
        public double Width
        {
            get { return this.width; }
            set { this.width = value; }
        }

        /// <summary>
        /// Gets or sets height of GameModel.
        /// </summary>
        /// <value>The Height of Window.</value>
        public double Height
        {
            get { return this.height; }
            set { this.height = value; }
        }
    }
}
