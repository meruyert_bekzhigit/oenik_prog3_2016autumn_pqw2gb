﻿// <copyright file="MainWindow.xaml.cs" company="Obuda"> 
//      Obuda copyright tag.
// </copyright>
// <summary>
//  <author>Meruyert Bekzhigit</author>
// </summary>
namespace Snowstorm
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for MainWindow.
    /// </summary>
    public partial class MainWindow : Window
    {        
        /// <summary>
        /// View Model of Project.
        /// </summary>
        private ViewModel vm;

        /// <summary>
        /// Game Model of Project.
        /// </summary>
        private GameModel gm;

        /// <summary>
        /// Game Logic of Project.
        /// </summary>
        private GameLogic logic;

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Window Loaded event handling part.
        /// </summary>
        /// <param name="sender">Object which fired event.</param>
        /// <param name="e">Arguments of Object.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.gm = new GameModel(this.Width, this.Height);
            this.vm = new ViewModel(this.gm, this.Width, this.Height);
            this.logic = new GameLogic(this.vm, this.gm);
            this.gameField.SetData(this.logic, this.vm, this.gm);
        }
    }
}
