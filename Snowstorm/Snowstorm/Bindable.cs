﻿// <copyright file="Bindable.cs" company="Obuda"> 
//      Obuda copyright tag.
// </copyright>
// <summary>
//  <author>Meruyert Bekzhigit</author>
// </summary>
namespace Snowstorm
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Binding class.
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// PropertyChangedEventHandler event handler.   
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Public function to call OnPropertyChanged to all Elements.
        /// </summary>
        public void Refresh()
        {
            this.OnPropertyChanged(string.Empty);
        }

        /// <summary>
        /// OnPropertyChanged which is going watch on changes of properties.
        /// </summary>
        /// <param name="name">Name of Property.</param>
        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
