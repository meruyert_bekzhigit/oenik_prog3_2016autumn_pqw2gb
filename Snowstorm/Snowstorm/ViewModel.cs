﻿// <copyright file="ViewModel.cs" company="Obuda"> 
//      Obuda copyright tag.
// </copyright>
// <summary>
//  <author>Meruyert Bekzhigit</author>
// </summary>
namespace Snowstorm
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Shapes;

    /// <summary>
    /// View Model.
    /// </summary>
    public class ViewModel : Bindable
    {
        /// <summary>
        /// Game Model.
        /// </summary>
        private GameModel model;

        /// <summary>
        /// Initializes a new instance of the ViewModel class.
        /// </summary>
        /// <param name="newModel">Game Model.</param>
        /// <param name="w">Window width.</param>
        /// <param name="h">Window height.</param>
        public ViewModel(GameModel newModel, double w, double h)
        {
            this.FirstTeam = new List<Player>();
            this.SecondTeam = new List<Player>();
            this.model = newModel;
            for (int i = 1; i <= this.model.FirstTeamPlayers; i++)
            {
                Player p = new Player((w / 2) + (i * 40), (2 * (h / 3)) - (i * 30), 20, 20);
                this.FirstTeam.Add(p);
            }

            for (int i = 1; i <= this.model.SecondTeamPlayers; i++)
            {
                Player p = new Player((w / 2) - (i * 60), (h / 5) + (i * 30), 20, 20);
                this.SecondTeam.Add(p);
            }
        }

        /// <summary>
        /// Gets a value indicating List of Player instances in First Team.
        /// </summary>
        /// <value>The List of Player.</value>
        public List<Player> FirstTeam { get; private set; }

        /// <summary>
        /// Gets a value indicating List of Player instances in Second Team.
        /// </summary>
        /// <value>The List of Player.</value>
        public List<Player> SecondTeam { get; private set; }
    }
}
