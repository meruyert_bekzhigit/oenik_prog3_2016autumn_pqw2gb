﻿// <copyright file="Player.cs" company="Obuda"> 
//      Obuda copyright tag.
// </copyright>
// <summary>
//  <author>Meruyert Bekzhigit</author>
// </summary>
namespace Snowstorm
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// States of Player.
    /// </summary>
    public enum States
    {
        /// <summary>
        /// When Player is in Rest state.
        /// </summary>
        Rest,

        /// <summary>
        /// When Player is in Moving state.
        /// </summary>
        Moving,

        /// <summary>
        /// When Player is in Shooting state.
        /// </summary>
        Shoot,

        /// <summary>
        /// When Player is in Falling state.
        /// </summary>
        FallDown
    }

    /// <summary>
    /// Player model description class.
    /// </summary>
    public class Player : Bindable
    {
        /// <summary>
        /// Rectangle area of figure.
        /// </summary>
        public Rect Area;

        /// <summary>
        /// Dictionary with Brush for each state.
        /// </summary>
        private Dictionary<States, Brush> dict;

        /// <summary>
        /// Declaring integer with number of Armor.
        /// </summary>
        private int armor;

        /// <summary>
        /// Declaring State with state of Player.
        /// </summary>
        private States state;

        /// <summary>
        /// Declaring SnowBall of Player.
        /// </summary>
        private SnowBall snowBall;

        /// <summary>
        /// Initializes a new instance of the Player class.
        /// </summary>
        /// <param name="x">X Position.</param>
        /// <param name="y">Y Position.</param>
        /// <param name="w">Width of Player.</param>
        /// <param name="h">Height of Player.</param>
        public Player(double x, double y, double w, double h)
        {
            this.Area = new Rect(x, y, w, h);
            this.armor = 3;
            this.State = States.Rest;
            this.dict = new Dictionary<States, Brush>() 
            {
                { States.Rest, Brushes.Gold },
                { States.Moving, Brushes.DarkOrchid },
                { States.Shoot, Brushes.DarkOrange },
                { States.FallDown, Brushes.White }
            };
            this.Refresh();
        }

        /// <summary>
        /// Gets or sets number of Armor of Player.
        /// </summary>
        /// <value>The number of Armor.</value>
        public int Armor
        {
            get { return this.armor; }
            set { this.armor = value; }
        }

        /// <summary>
        /// Gets or sets state of Player.
        /// </summary>
        /// <value>The state of Player.</value>
        public States State
        {
            get { return this.state; }
            set { this.state = value; }
        }

        /// <summary>
        /// Gets or sets Snowball of Player.
        /// </summary>
        /// <value>The SnowBall.</value>
        public SnowBall SnowBall
        {
            get { return this.snowBall; }
            set { this.snowBall = value; }
        }

        /// <summary>
        ///  Gets a value indicating whether Player is still Alive or not.
        /// </summary>
        /// <value>The boolean to check if Player is Alive.</value>
        public bool IsAlive
        {
            get
            {
                return this.Armor > 0 ? true : false;
            }
        }

        /// <summary>
        /// Gets a value indicating Brush of Player.
        /// </summary>
        /// <value>The Brush.</value>
        public Brush Brush
        {
            get
            {
                return this.dict[this.State]; //// new ImageBrush(new BitmapImage(new Uri(dict[this.State], UriKind.Relative)));
            }
        }

        /// <summary>
        /// Function to instantiating SnowBall for Player.
        /// </summary>
        public void CreateBall()
        {
            this.SnowBall = new SnowBall(this.Area.X + this.Area.Width, this.Area.Y, 10);
        }
    }
}
