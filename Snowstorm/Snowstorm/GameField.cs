﻿// <copyright file="GameField.cs" company="Obuda"> 
//      Obuda copyright tag.
// </copyright>
// <summary>
//  <author>Meruyert Bekzhigit</author>
// </summary>
namespace Snowstorm
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Framework Element - Game Field.
    /// </summary>
    public class GameField : FrameworkElement
    {
        /// <summary>
        /// Game Logic.
        /// </summary>
        private GameLogic logic;

        /// <summary>
        /// View Model.
        /// </summary>
        private ViewModel viewModel;

        /// <summary>
        /// Game Model.
        /// </summary>
        private GameModel gameModel;

        /// <summary>
        /// Point which stores Mouse coordinates.
        /// </summary>
        private Point mousePoint;

        /// <summary>
        /// Boolean to check whether Player is selected or not.
        /// </summary>
        private bool isPlayerSelected;

        /// <summary>
        /// Initializes a new instance of the GameField class.
        /// </summary>
        public GameField()
        {
            this.MouseDown += this.GameField_MouseDown;
            this.MouseMove += this.GameField_MouseMove;
            this.MouseUp += this.GameField_MouseUp;
        }

        /// <summary>
        /// Function to set Date from outside.
        /// </summary>
        /// <param name="logic">Game Logic for Game Field.</param>
        /// <param name="viewModel">View Model for Game Field.</param>
        /// <param name="gameModel">Game Model for Game Field.</param>
        public void SetData(GameLogic logic, ViewModel viewModel, GameModel gameModel)
        {
            this.logic = logic;
            this.viewModel = viewModel;
            this.gameModel = gameModel;
            this.viewModel.PropertyChanged += this.ViewModel_PropertyChanged;
            this.InvalidateVisual();
        }

        /// <summary>
        /// On Render function of Framework Element.
        /// </summary>
        /// <param name="drawingContext">Drawing Context.</param>
        protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
        {
            if (this.gameModel != null)
            {
                drawingContext.DrawLine(new Pen(Brushes.LightCyan, 3), new Point(this.gameModel.Width / 2, 0), new Point(this.gameModel.Width / 2, this.gameModel.Height));
            }

            if (this.viewModel != null)
            {
                foreach (Player player in this.viewModel.FirstTeam)
                {
                    drawingContext.DrawRectangle(player.Brush, new Pen(Brushes.Black, 2), player.Area);
                    drawingContext.DrawText(new FormattedText(player.Armor.ToString() + " Armor", System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Cambria"), 14, Brushes.Green), new Point(player.Area.X - 15, player.Area.Y - 27));

                    if (player.SnowBall != null)
                    {
                        EllipseGeometry snowBallGeometry = new EllipseGeometry(player.SnowBall.Area);
                        drawingContext.DrawGeometry(player.SnowBall.Brush, new Pen(Brushes.DarkGray, 2), snowBallGeometry);
                        drawingContext.DrawText(new FormattedText(player.SnowBall.Power.ToString() + "x", System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Cambria"), 14, Brushes.CadetBlue), new Point(player.Area.X + 25, player.Area.Y + 2));
                    }
                }

                foreach (Player player in this.viewModel.SecondTeam)
                {
                    drawingContext.DrawRectangle(player.Brush, new Pen(Brushes.Brown, 2), player.Area);
                    drawingContext.DrawText(new FormattedText(player.Armor.ToString() + " Armor", System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Cambria"), 14, Brushes.Red), new Point(player.Area.X - 15, player.Area.Y - 27));
                    
                    if (player.SnowBall != null)
                    {
                        EllipseGeometry snowBallGeometry = new EllipseGeometry(player.SnowBall.Area);
                        drawingContext.DrawGeometry(player.SnowBall.Brush, new Pen(Brushes.DarkGray, 2), snowBallGeometry);
                        drawingContext.DrawText(new FormattedText(player.SnowBall.Power.ToString() + "x", System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Cambria"), 14, Brushes.CadetBlue), new Point(player.Area.X, player.Area.Y + player.Area.Height));
                    }
                }
            }
        }

        /// <summary>
        /// Mouse Move event handling.
        /// </summary>
        /// <param name="sender">Object which fires event.</param>
        /// <param name="e">Arguments of Objects.</param>
        private void GameField_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.mousePoint = e.GetPosition(this);
            if (this.isPlayerSelected)
            {
                this.logic.MovePlayer(this.mousePoint);
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// Mouse Down event handling.
        /// </summary>
        /// <param name="sender">Object which fires event.</param>
        /// <param name="e">Arguments of Objects.</param>
        private void GameField_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.mousePoint = e.GetPosition(this);
            if (!this.isPlayerSelected)
            {
                //// If player is selected then method returns true and false in other case.
                if (this.logic.SelectPlayer(this.mousePoint))
                {
                    this.isPlayerSelected = true;
                }
            }
        }

        /// <summary>
        /// Mouse Up event handling.
        /// </summary>
        /// <param name="sender">Object which fires event.</param>
        /// <param name="e">Arguments of Objects.</param>
        private void GameField_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.isPlayerSelected)
            {
                //// If player is unselected then method returns true and false in other case.
                if (this.logic.UnselectPlayerAndShoot())
                {
                    this.isPlayerSelected = false;
                }
            }
        }

        /// <summary>
        /// Model Property Changed event handling.
        /// </summary>
        /// <param name="sender">Object which fires event.</param>
        /// <param name="e">Arguments of Objects.</param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            this.InvalidateVisual();
        }
    }
}
