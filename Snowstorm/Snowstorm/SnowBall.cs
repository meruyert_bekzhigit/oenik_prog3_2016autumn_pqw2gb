﻿// <copyright file="SnowBall.cs" company="Obuda"> 
//      Obuda copyright tag.
// </copyright>
// <summary>
//  <author>Meruyert Bekzhigit</author>
// </summary>
namespace Snowstorm
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Snowball model description class, snowball for player.
    /// </summary>
    public class SnowBall : Bindable
    {
        /// <summary>
        /// Variable to store Horizontal speed of Snowball.
        /// </summary>
        public const double SpeedX = 10;

        /// <summary>
        /// Variable to store Vertical speed of Snowball.
        /// </summary>
        public const double SpeedY = 10;

        /// <summary>
        ///  Declaring Rectangle Area of SnowBall.
        /// </summary>
        public Rect Area;

        /// <summary>
        /// Declaring integer with power of Snowball.
        /// </summary>
        private int power;

        /// <summary>
        /// Initializes a new instance of the SnowBall class.
        /// </summary>
        /// <param name="x">X-axis Position.</param>
        /// <param name="y">Y-axis Position.</param>
        /// <param name="r">Radius of SnowBall.</param>
        public SnowBall(double x, double y, double r)
        {
            this.Area = new Rect(x, y, r, r);
            this.Power = 1;
        }

        /// <summary>
        /// Gets or sets value of Power of Player.
        /// </summary>
        /// <value>The value of Power.</value>
        public int Power
        {
            get { return this.power; }
            set { this.power = value; }
        }

        /// <summary>
        /// Gets a value indicating Brush of SnowBall.
        /// </summary>
        /// <value>The Brush.</value>
        public Brush Brush
        {
            get
            {
                return Brushes.LightGray;
            }
        }
    }
}
